
CREATE TABLE IF NOT EXISTS options(
    name VARCHAR(30) NOT NULL,
    value VARCHAR(30),
    primary key (name),
    UNIQUE (name)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE IF NOT EXISTS articles(
    id smallint unsigned NOT NULL auto_increment,
    name VARCHAR(155) NOT NULL,
    article TEXT,
    primary key (id),
    UNIQUE (name)
) DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO options(name,value) values('template','debug');
INSERT INTO options(name,value) values('sitename','NEWS arhive');


#Файл: default.sql
#Версія: 1.0.0
#Призначення: Запит створення БД