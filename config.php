<?php

/*
// Файл: config.php
// Версія: 1.0.0
// Призначення: Загальні налаштування
*/

    /*********** Загальні налаштування ***********/ 
    ini_set("display_errors",true);
    ini_set("max_execution_time", "60");
    header("Content-Type:text/html;charset=UTF-8");
    global $home_url;
    $home_url = 'http://localhost/newsite/';
    global $path_home;
    $path_home = dirname(__FILE__);
    error_reporting(0);
    /***********END Загальні налаштування ***********/ 


?>