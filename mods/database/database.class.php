<?php

/*
// Файл: database.class
// Версія: 1.0.0
// Призначення: Клас бази даних
*/

class data_base {
    public $localhost;
    public $user;
    public $password;
    public $dbname;
    public $def_sql_file;
    
    public function db_connection($localhost,$user,$password,$dbname,$def_sql_file,$auto_create) {
        /*
         * Підключення до БД
         * Вхідні дані: $localhost,$user,$password,$dbname,$def_sql_file
         * Вихідні дані: return $mysqli;       
         */
        $mysqli = @new mysqli($localhost,$user,$password,$dbname);
	if (mysqli_connect_errno()) {
            echo "Помилка підключення до бази даних!";
            exit;
	}
        
        $request = "SELECT * FROM options;";
        if (!$this->sql_request($mysqli,$request)) {
            if ($auto_create == true) {
                $this->create_db($mysqli,$def_sql_file);
            } else {
                echo 'Помилка бази даних! Немає всіх таблиць!';
                exit();
            }
        }
        $request = "SELECT * FROM articles;";
        if (!$this->sql_request($mysqli,$request)) {
            if ($auto_create == true) {
                $this->create_db($mysqli,$def_sql_file);
            } else {
                echo 'Помилка бази даних! Немає всіх таблиць!';
                exit();
            }
        }
        $mysqli->set_charset("utf8");
        return $mysqli;
    }
    
    public function db_connection_close($mysqli) {
        /*
         * Розрив з'єднання з БД
         * Вхідні дані: $localhost,$user,$password,$dbname
         * Вихідні дані: return bool;
         */
        
        if (isset($mysqli)) {
            $result = $mysqli->close();
            if ($result) return TRUE;
               else return FALSE;
	}
    }
    
    private function create_db($mysqli,$def_sql_file) {
        /*
         * Створення таблиць БД з файлу
         * Вхідні дані: $mysqli,$def_sql_file
         * Вихідні дані: return bool;
         */
        //$request = str_replace('\n', '', $mysqli->real_escape_string(file_get_contents($def_sql_file)));
        $request = str_replace('\n', '', file_get_contents($def_sql_file));
        if (isset($mysqli)) {
            $result = $mysqli->multi_query($request);
         }
         echo 'Таблиці створено. Перезавантажте сторінку!';
         exit();
        //if ($result) return TRUE;
        //else return FALSE;
    }
    
    public function sql_request($mysqli,$request) {
        /*
         * Надсилання запиту до БД
         * Вхідні дані: $mysqli,$request
         * Вихідні дані: return $result;
         */
        if (isset($mysqli)) {
            $result = $mysqli->query($request);
	}
        
        return $result;
    }
}

?>