<?php

/*
// Файл: auth.php
// Версія: 1.0.0
// Призначення: Файл форми авторизації
*/
$sitename = 'Додавання статті';
$footer = $sitename;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="author" content="">

    <title><?php echo $sitename; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $GLOBALS["home_url"]; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $GLOBALS["home_url"]; ?>bootstrap/jumbotron-narrow.css" rel="stylesheet">
    <script src="<?php echo $GLOBALS["home_url"]; ?>jquery/jquery-3.1.1.min.js"></script>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
      <div class="header">
        <ul class="nav nav-pills pull-right">
          <li class="active"><a href="<?php echo $GLOBALS["home_url"]; ?>addarticle.php">Додати статтю</a></li>
          <li class="active"><a href="<?php echo $GLOBALS["home_url"]; ?>">Головна</a></li>
        </ul>
        <h3 class="text-muted"><?php echo $sitename; ?></h3>
      </div>

      <div class="jumbotron">
        <form action="" method="post"> 
            <?php echo (isset($no_autorization)) ? $no_autorization : ''; ?>
            Логін : <input type="text" class="form-control" name="username"><br> 
            Пароль : <input type="password" class="form-control" name="password"><br> 
            <input type="submit" class="btn btn-success" value="Надіслати"> 
        </form>
      </div>

      <div class="footer">
        <p>&copy; <i><?php echo $footer; ?></i></p>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>