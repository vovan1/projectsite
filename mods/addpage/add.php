<?php

/*
// Файл: add.php
// Версія: 1.0.0
// Призначення: Файл форми додавання статті
*/

$sitename = 'Додавання статті';
$footer = $sitename;
        
        session_start();
	$_SESSION['username'] = $_POST['username'];
	$_SESSION['password'] = $_POST['password'];
        
        $add_article = '';
        if (isset($_POST['name_text'])) {
            $name_text = htmlspecialchars($_POST['name_text']);
        } else {
            $name_text = '';
        }
        if (isset($_POST['article_text'])) {
            $article_text = htmlspecialchars($_POST['article_text']);
        } else {
            $article_text = '';
        }
        $perenos_checked = true;
        if (isset($_POST['perenos'])) {
            $perenos = (bool)$_POST['perenos'];
            ($perenos == false) ? $perenos_checked = false : '';
        } else {
            $perenos = false;
            ($perenos == false) ? $perenos_checked = false : '';
        }
        
        if ($perenos) {
            (isset($article_text)) ? $article_text = preg_replace( "/\n/" , "\n <br /><br />" , $article_text) : '';
        }
        
        if ($name_text != "" && $article_text != "") {
            $sql = "INSERT INTO 
		articles(name,article) 
		values('" . $name_text . "','" .$article_text . "');";	
            
            $result = $database->sql_request($mysqli,$sql);
		if ($result) {
			$add_article = "<p style='color: green;'>Стаття: <b>" .  $name_text . "</b> - ОПУБЛІКОВАНА! </p>";
		}
		else {
			$add_article = "<p style='color: red;'>Стаття: <b>" . $name_text . "</b> - НЕ ОПУБЛІКОВАНА! </p>";
		}
        }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="author" content="">

    <title><?php echo $sitename; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $GLOBALS["home_url"]; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $GLOBALS["home_url"]; ?>bootstrap/jumbotron-narrow.css" rel="stylesheet">
    <script src="<?php echo $GLOBALS["home_url"]; ?>jquery/jquery-3.1.1.min.js"></script>
    <script type="text/javascript">

	function valid() {
		if ((document.getElementById("name_text").value == '')||(document.getElementById("article_text").value == '')) {
			alert('Заповніть всі поля!');
		}
	}
    </script>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
      <div class="header">
        <ul class="nav nav-pills pull-right">
           <li class="active"><a href="<?php echo  $GLOBALS['home_url']; ?>addarticle.php">Вийти</a></li>
          <li class="active"><a href="<?php echo $GLOBALS["home_url"]; ?>">Головна</a></li>
        </ul>
          <h3 class="text-muted"><?php echo $sitename; ?> - (<i><?php echo $_SESSION['username']; ?></i>)</h3>
      </div>

      <div class="jumbotron">
            <form name="add_news" method="post" action="">
                <?php echo (isset($add_article)) ? $add_article : ''; ?>
                <h2>Додавання статті</h2>
                    <table width="100%">
                            <tr>
                                    <td>Назва <font color="red">*</font>:</td>
                                    <td><input type="text" id="name_text" class="form-control" name="name_text" value="<?php echo (isset($_POST['name_text']))?$_POST['name_text']:'';?>" size="69" /></td>
                            </tr>
                            <tr>
                                    <td>Текст <font color="red">*</font>:</td>
                                    <td><textarea size="50" id="article_text" class="form-control" name="article_text" cols="52" rows="5"><?php echo (isset($_POST['article_text']))?$_POST['article_text']:'';?></textarea></td>
                            </tr>
                            <tr>
                                <td colspan="2"><i><font color="red">*</font> - поля, які обов'язково заповнюються</i><br /><label><input type="checkbox" class="form-check-input" name="perenos" <?php echo ($perenos_checked==false) ? '' : 'checked="true";'?> /><i>Автоматичне перенесення рядків</i></label></td>
                            </tr>
                            <tr>
                                    <td colspan="2" align="center"><input type="submit" class="btn btn-success"  value="Опублікувати" onClick="valid();" /></td>
                            </tr>
            
                    </table>

                    <input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>" />
                    <input type="hidden" name="password" value="<?php echo $_SESSION['password']; ?>" />

            </form>
            <br />
            <form method="post" action="">
            <input type="hidden" name="username" value="<?php echo $_SESSION['username']; ?>" />
            <input type="hidden" name="password" value="<?php echo $_SESSION['password']; ?>" />
            <input type="submit" class="btn btn-primary" value="Очистити все"/></a>
            </form>					
      </div>

      <div class="footer">
        <p>&copy; <i><?php echo $footer; ?></i></p>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
