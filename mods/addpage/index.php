<?php

/*
// Файл: index.php
// Версія: 1.0.0
// Призначення: Файл форми авторизації
*/
if (isset($_POST['password'])&&isset($_POST['username'])) {
        if ($_POST['password'] != $password || $_POST['username'] != $username) { 
            $no_autorization = '<p style="color: red;"><i>Неправильна пара логін/пароль!</i></p>';
            require_once $GLOBALS['path_home'] . "/mods/addpage/auth.php";
        } else if ($_POST['password'] == $password && $_POST['username'] == $username) { 
            require_once $GLOBALS['path_home'] . "/mods/addpage/add.php";
        }
    } else {
        require_once $GLOBALS['path_home'] . "/mods/addpage/auth.php";
    }
?>