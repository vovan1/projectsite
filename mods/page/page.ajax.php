<?php

/*
// Файл: page.ajax.php
// Версія: 1.0.0
// Призначення: Вивід асинхронно підвантажених сторінок
*/
     header('Content_Type: text/html'); 

 if (isset($_POST['count_viewed_articles'])&&isset($_POST['count_articles'])&&isset($_POST['psession'])) {
     
    //Дешифрування адреси скрипта
    $path_config = $_POST['psession'];
    $path_config = str_replace('@@@@@', '-', $path_config);
    $path_config = str_replace('!_u_!', 's', $path_config);
    $path_config = str_replace('!Z!', 'C', $path_config);
    $path_config = str_replace('!X!', 'D', $path_config);
    $path_config = str_replace('!_!_!', '/', $path_config);
    $path_config = str_replace('!_?_!', ':', $path_config);
    $path_config = strrev($path_config);
    
    $path_config_slash = $path_config;
    $path_document_root_slash = $_SERVER['DOCUMENT_ROOT'];
    
    $path_config_slash = str_replace('/', '', $path_config_slash);
    $path_config_slash = str_replace('\\', '', $path_config_slash);     
    $path_document_root_slash = str_replace('/', '', $path_document_root_slash);
    $path_document_root_slash = str_replace('\\', '', $path_document_root_slash);    
    $path_document_root_slash = substr($path_document_root_slash,2);
    
    if (strpos($path_config_slash,$path_document_root_slash)===0) {
        echo 'Помилка виконання! Спробуйте перезавантажити сторінку!';
        exit();
    }
     //END Дешифрування адреси скрипта
     
    /*********** Підключаєм налаштування і файли ***********/ 
    require_once($path_config.'/config.php');
    require_once($GLOBALS["path_home"].'/dbconfig.php');
    require_once($GLOBALS["path_home"].'/mods/database/database.class.php');
    /***********END Підключаєм налаштування і файли ***********/
    
     header('Content_Type: text/html'); 
    
    /*********** Встановлюєм з'єднання з БД ***********/ 
    $database = new data_base();
    $mysqli = $database->db_connection($localhost,$user,$password,$dbname,$def_sql_file,$auto_create);
    /***********END Встановлюєм з'єднання з БД ***********/
    
    /*********** Записуєм значення глобального масиву GET в змінну ***********/
        $count_viewed_articles = $_POST['count_viewed_articles'];
        $count_viewed_articles = intval(trim(htmlentities($count_viewed_articles)));
        $count_viewed_articles = preg_replace("/[^0-9]/i", "", $count_viewed_articles);
        $count_viewed_articles += 1;
        
        $count_articles = $_POST['count_articles'];
        $count_articles = intval(trim(htmlentities($count_articles)));
        $count_articles = preg_replace("/[^0-9]/i", "", $count_articles);
        $view = intval($count_articles)-intval($count_viewed_articles);
        if (isset($database)) {
                $result = $database->sql_request($mysqli,"SELECT * FROM `articles` ORDER BY `id` DESC LIMIT " . $count_viewed_articles . "," . intval($view) . ";");
                $i=0;
                while ($row = $result->fetch_row()) {
                    $articles['id'][$i] = html_entity_decode($row[0]);
                    $articles['name'][$i] = html_entity_decode($row[1]);
                    $i++;
                }   
        }
        
        $articles_html = "";

        for ($i = 0; $i<$view ;$i++) {
            $articles_html = $articles_html . "<li class=\"list-group-item\"><a href=\"" . $GLOBALS["home_url"] . "index.php?id=" . $articles['id'][$i] . "\"><b>" . $articles['name'][$i] . "</b></a></li>\n";
         }
         
        /*********** Завершуєм з'єднання з БД ***********/ 
        if (isset($mysqli)) {
            $database->db_connection_close($mysqli);
        }
        /***********END Завершуєм з'єднання з БД ***********/
        
        echo $articles_html;
        
} else {
    echo "Немає записів!"; 
}
    /***********END Записуєм значення глобального масиву GET в змінну ***********/
?>