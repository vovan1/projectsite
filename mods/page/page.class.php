<?php

/*
// Файл: page.class.php
// Версія: 1.0.0
// Призначення: Клас сторінки
*/

class page {
    public $file;
    public $template;
    
    function __construct($file,$template) {
        /*
         * Задання значень атрибутам
         * Вхідні дані: $file,$template
         * Вихідні дані: -      
         */
       $this->file = $file;
       $this->template = $template;
   }
   
   public function page_home($database,$mysqli) {
        /*
         * Головна сторінка
         * Вхідні дані: $file,$template
         * Вихідні дані: return string;      
         */
        if (!file_exists($this->file)) {
            echo 'Неправильний шлях до файлу сторінки!';
            exit;
        }
        require_once $this->file;
        require_once $this->template.'/home.php';
   }
   public function page_article($id,$database,$mysqli) {
        /*
         * Сторінка статті
         * Вхідні дані: $file,$template
         * Вихідні дані: return string;     
         */
        if (!file_exists($this->file)) {
            echo 'Неправильний шлях до файлу сторінки!';
            exit;
        }
        require_once $this->file;
        require_once $this->template.'/article.php';
        
   }
   public function page_error($database,$mysqli) {
        /*
         * Сторінка помилки
         * Вхідні дані: $file,$template
         * Вихідні дані: return string;     
         */
        if (!file_exists($this->file)) {
            echo 'Неправильний шлях до файлу сторінки!';
            exit;
        }
        require_once $this->file;
        require_once $this->template.'/error.php';
   }
}

?>