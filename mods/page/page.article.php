<?php

/*
// Файл: page.article.php
// Версія: 1.0.0
// Призначення: Файл для задання змінних для статті
*/

    /*********** Отримуєм дані сторінки ***********/ 
    if (isset($database)) {
        $result = $database->sql_request($mysqli,"SELECT `value` FROM `options` WHERE `name` LIKE 'sitename';");
        while ($row = $result->fetch_row()) {
            $sitename = html_entity_decode($row[0]);
        }   
    }
    /***********END Отримуєм дані сторінки ***********/ 
    
    /*********** Отримуєм статтю ***********/ 
    if (isset($database)) {
        $result = $database->sql_request($mysqli,"SELECT name,article FROM `articles` WHERE `id` = " . $id . ";");
        while ($row = $result->fetch_row()) {
            $article['name'] = html_entity_decode($row[0]);
            $article['article'] = html_entity_decode($row[1]);
        }   
    }
    /***********END Отримуєм статтю ***********/ 
?>