<?php

/*
// Файл: page.home.php
// Версія: 1.0.0
// Призначення: Файл для задання змінних головної сторінки
*/

    /*********** Отримуєм дані сторінки ***********/ 
    if (isset($database)) {
        $result = $database->sql_request($mysqli,"SELECT `value` FROM `options` WHERE `name` LIKE 'sitename';");
        while ($row = $result->fetch_row()) {
            $sitename = html_entity_decode($row[0]);
        }   
    }
    /***********END Отримуєм дані сторінки ***********/ 
    
    /*********** Отримуєм статті ***********/ 
    if (isset($database)) {
        $result = $database->sql_request($mysqli,"SELECT * FROM `articles` ORDER BY `id` DESC;");
        $i = 0;
        while ($row = $result->fetch_row()) {
            $articles['id'][$i] = html_entity_decode($row[0]);
            $articles['name'][$i] = html_entity_decode($row[1]);
            $i++;
        }   
    }
    /***********END Отримуєм статті ***********/ 
?>