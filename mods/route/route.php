<?php

/*
// Файл: route.php
// Версія: 1.0.0
// Призначення: Перенаправлення
*/

class router {
    public $parametrs;
    public $template;
    
    
    public function route_template($database,$mysqli) {
        if (isset($database)) {
            $result = $database->sql_request($mysqli,"SELECT `value` FROM `options` WHERE `name` LIKE 'template';");
           while ($row = $result->fetch_row()) {
                $template = $row[0];
            }   
        }
        return $template;
    }
    
    public function route($parametrs,$template,$page_home_path,$page_article_path,$page_error_path,$database,$mysqli) {
        /* Визначення сторінки
         * Вхідні дані: $parametrs
         * Вихідні дані: return string;
         */
        $parametrs_count=0;
        foreach ($_GET as $key => $value) {
           $parametrs_count++;
        }
        
        if ($parametrs_count == 0 && $parametrs == NULL) {
            $this->route_home($page_home_path,$template,$database,$mysqli);
            return 'home';
        } 
        else if (isset($parametrs['id']) && $parametrs_count==1) {
            
            $id = $parametrs['id'];
            $id = intval(trim(htmlentities($id)));
            $id = preg_replace("/[^0-9]/i", "", $id);
            if (isset($database)) {
                $result = $database->sql_request($mysqli,"SELECT `id` FROM `articles` WHERE `id` = " . $id . ";");
                while ($row = $result->fetch_row()) {
                $id_sql = $row[0];
                }   
            }
            
            if (isset($id_sql)) {
                $this->route_article($id_sql,$page_article_path,$template,$database,$mysqli);
                return 'article';
            }
            else {
                $this->route_error($page_error_path,$template,$database,$mysqli);
                return 'error';
            }
            
        }
        else {
            $this->route_error($page_error_path,$template,$database,$mysqli);
            return 'error';
        }
    }
    
    private function route_article($id,$page_path,$template,$database,$mysqli) {
        /* Завантаження статті
         * Вхідні дані: $id
         * Вихідні дані: -
         */
        $page = new page($page_path,$template);
        $page->page_article($id,$database,$mysqli);
    }
    private function route_home($page_path,$template,$database,$mysqli) {
        /* Завантаження головної
         * Вхідні дані: -
         * Вихідні дані: -
         */
        $page = new page($page_path,$template);
        $page->page_home($database,$mysqli);
    }
    private function route_error($page_path,$template,$database,$mysqli) {
        /* Завантаження сторінки помилки
         * Вхідні дані: -
         * Вихідні дані: -
         */
        $page = new page($page_path,$template);
        $page->page_error($database,$mysqli);
    }
}

?>