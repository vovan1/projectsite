<?php

/*
// Файл: index.php
// Версія: 1.0.0
// Призначення: Головний індексний файл 
*/

    /*********** Підключаєм налаштування і файли ***********/ 
    require_once(dirname(__FILE__).'/config.php');
    require_once(dirname(__FILE__).'/dbconfig.php');
    require_once(dirname(__FILE__).'/mods/database/database.class.php');
    require_once(dirname(__FILE__).'/mods/page/page.class.php');
    require_once(dirname(__FILE__).'/mods/route/route.php');
    $page_home_path = dirname(__FILE__).'/mods/page/page.home.php';
    $page_article_path = dirname(__FILE__).'/mods/page/page.article.php';
    $page_error_path = dirname(__FILE__).'/mods/page/page.error.php';
    /***********END Підключаєм налаштування і файли ***********/ 
 
    /*********** Встановлюєм з'єднання з БД ***********/ 
    $database = new data_base();
    $mysqli = $database->db_connection($localhost,$user,$password,$dbname,$def_sql_file,$auto_create);
    /***********END Встановлюєм з'єднання з БД ***********/
    
    /*********** Записуєм значення глобального масиву GET в змінну ***********/ 
    if (isset($_GET)) {
        foreach ($_GET as $key => $value) {
            $get_data[$key] = $value;
        }
    }
    /***********END Записуєм значення глобального масиву GET в змінну ***********/ 
    
    
    /*********** Визначаємо яку сторінку завантажувати і який шаблон ***********/ 
    $router = new router();
    $template = $router->route_template($database,$mysqli);
    if (!file_exists((dirname(__FILE__).'/templates/'.$template))) {
        echo 'Неправильний шлях до шаблону!';
        exit;
    } else {
        $template = dirname(__FILE__).'/templates/'.$template;
    }
    if (!isset($get_data)) {
        $get_data = NULL;
    } 
    
    $router->route($get_data,$template,$page_home_path,$page_article_path,$page_error_path,$database,$mysqli);
    /*********** Визначаємо яку сторінку завантажувати і який шаблон ***********/ 
    
    
    /*********** Завершуєм з'єднання з БД ***********/ 
    if (isset($mysqli)) {
        $database->db_connection_close($mysqli);
    }
    /***********END Завершуєм з'єднання з БД ***********/ 
?>