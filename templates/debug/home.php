<?php

/*
// Файл: home.php
// Версія: 1.0.0
// Призначення: Головна сторінка
*/
$footer = $sitename;
$description = $sitename;
// Шифрування головної адреси скрипта для передачі в page.ajax.php (xampp)
$psession = strrev($GLOBALS['path_home']);
$psession = str_replace(':', '!_?_!', $psession);
$psession = str_replace('/', '!_!_!', $psession);
$psession = str_replace('D', '!X!', $psession);
$psession = str_replace('C', '!Z!', $psession);
$psession = str_replace('s', '!_u_!', $psession);
$psession = str_replace('-', '@@@@@', $psession);
//END Шифрування головної адреси скрипта для передачі (xampp)

if (isset($articles['id'])) {
    $count_articles = count($articles['id'], COUNT_RECURSIVE);
} else {
    $count_articles = 0;
}
$count_view_articles = 5;
$count_viewed_articles = $count_view_articles;

$articles_html = "";

if ($count_articles <=$count_view_articles) {
    $count_viewed_articles = $count_articles;
    for ($i = 0; $i<$count_articles;$i++) {
        $articles_html = $articles_html . "<li class=\"list-group-item\"><a href=\"" . $GLOBALS["home_url"] . "index.php?id=" . $articles['id'][$i] . "\"><b>" . $articles['name'][$i] . "</b></a></li>\n";
    }
} else {
    for ($i = 0; $i<$count_view_articles;$i++) {
        $articles_html = $articles_html . "<li class=\"list-group-item\"><a href=\"" . $GLOBALS["home_url"] . "index.php?id=" . $articles['id'][$i] . "\"><b>" . $articles['name'][$i] . "</b></a></li>\n";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $description; ?>">
    <meta name="author" content="">

    <title><?php echo $sitename; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $GLOBALS["home_url"]; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $GLOBALS["home_url"]; ?>bootstrap/jumbotron-narrow.css" rel="stylesheet">
    <script src="<?php echo $GLOBALS["home_url"]; ?>jquery/jquery-3.1.1.min.js"></script>
    <script src="<?php echo $GLOBALS["home_url"]; ?>jquery/function.js"></script>
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">
      <div class="header">
        <ul class="nav nav-pills pull-right">
          <li class="active"><a href="<?php echo $GLOBALS["home_url"]; ?>addarticle.php">Додати статтю</a></li>
          <li class="active"><a href="<?php echo $GLOBALS["home_url"]; ?>">Головна</a></li>
        </ul>
        <h3 class="text-muted"><?php echo $sitename; ?></h3>
      </div>

      <div class="jumbotron">
        <h2>Статті сайту</h2>
        <sup>Відображено: <b id="count_viewed_articles"><?php echo $count_viewed_articles; ?></b> з <b id="count_articles"><?php echo $count_articles; ?></b></sup>
        <?php echo ($count_articles == 0) ? '<p>Немає публікацій!</p>' : ''; ?>
        <p><ol><?php echo $articles_html; ?><span id="ajax_articles"></span></ol></p>
        <p><a class="btn btn-lg btn-success" href="#" id="load_articles" role="button" onclick="get();">Завантажити всі статі</a></p>
        
      </div>

      <div class="footer">
        <p>&copy; <i><?php echo $footer; ?></i></p>
      </div>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <input type="hidden" id="home_url" value="<?php echo $GLOBALS["home_url"]; ?>" /> 
    <input type="hidden" id="psession" value="<?php echo $psession; ?>" /> 
  </body>
</html>