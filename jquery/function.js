
function get() {
    validate();
    count_viewed_articles = Number(document.getElementById("count_viewed_articles").innerHTML);
    count_articles = Number(document.getElementById("count_articles").innerHTML); 
    if (count_viewed_articles == count_articles) {
        alert('Більше немає статтей!');
    }
    else {
        psession = document.getElementById("psession").value; 
        send(count_viewed_articles,count_articles,psession);
    }
}

function validate() {
	document.getElementById("count_viewed_articles").innerHTML = document.getElementById("count_viewed_articles").innerHTML.replace(/[^\d]*/g, '');
        document.getElementById("count_articles").innerHTML = document.getElementById("count_articles").innerHTML.replace(/[^\d]*/g, '');
}

function send(count_viewed_articles,count_articles,psession) { 
        count_viewed_articles = count_viewed_articles-1;
	$.ajax({
		type: 'POST',
		url: document.getElementById("home_url").value+'mods/page/page.ajax.php',
		data: 'count_viewed_articles='+encodeURIComponent(count_viewed_articles)+'&count_articles='+encodeURIComponent(count_articles)+'&psession='+encodeURIComponent(psession),
		success: function(data){
			$('#ajax_articles').html(data);
		}
	});
        update(count_articles);
}

function update(){
    $('#load_articles').hide();
    $('#count_viewed_articles').html(count_articles);
}
