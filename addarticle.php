<?php

/*
// Файл: addarticle.php
// Версія: 1.0.0
// Призначення: Файл підключенняі додавання статті 
*/

    /*********** Підключаєм налаштування і файли ***********/ 
    require_once(dirname(__FILE__).'/config.php');
    require_once(dirname(__FILE__).'/dbconfig.php');
    require_once(dirname(__FILE__).'/mods/database/database.class.php');
    /***********END Підключаєм налаштування і файли ***********/ 
 
    /*********** Встановлюєм з'єднання з БД ***********/ 
    $database = new data_base();
    $mysqli = $database->db_connection($localhost,$user,$password,$dbname,$def_sql_file,$auto_create);
    /***********END Встановлюєм з'єднання з БД ***********/
    
    /*********** Дані для авторизації ***********/ 
    $username = 'admins';
    $password = 'passwordd';
    /***********END Дані для авторизації ***********/ 
    
    require_once(dirname(__FILE__).'/mods/addpage/index.php');
    
    
    /*********** Завершуєм з'єднання з БД ***********/ 
    if (isset($mysqli)) {
        $database->db_connection_close($mysqli);
    }
    /***********END Завершуєм з'єднання з БД ***********/ 
?>