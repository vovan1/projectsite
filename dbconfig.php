<?php

/*
// Файл: dbconfig.php
// Версія: 1.0.0
// Призначення: Дані підключення до БД
*/

    /*********** Дані підключення до БД ***********/ 
    $localhost = "localhost"; 
    $user = "root";
    $password = "";
    $dbname = "sitebd";
    /***********END Дані підключення до БД ***********/ 
    $def_sql_file = dirname(__FILE__).'/default.sql';//файл структури БД
    $auto_create = true;

?>